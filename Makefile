CC = gcc-10

BUILDDIR = build
OBJDIR = $(BUILDDIR)/obj
DEPDIR = $(BUILDDIR)/dep

ARGS ?=

WARNINGS = $(ERROR) -Wall -Wextra -Wconversion -Wcast-qual -Wwrite-strings -Wstrict-prototypes -Wformat=2 -Winit-self -Wmissing-include-dirs -Wswitch-default -Wfloat-equal -Wundef -Wunsafe-loop-optimizations -Wnested-externs -Wbad-function-cast -Wcast-align -Wwrite-strings -Wlogical-op -Wold-style-definition -Wmissing-prototypes -Wmissing-declarations -Wnormalized=nfkc -Wredundant-decls -Winline -Winvalid-pch -Wdisabled-optimization -Wpedantic -Wformat-overflow=2 -Wformat-truncation=2 -Wformat-signedness -Wsuggest-attribute=cold -Wsuggest-attribute=const -Wsuggest-attribute=format -Wsuggest-attribute=malloc -Wsuggest-attribute=noreturn -Wsuggest-attribute=pure -Wsuggest-final-methods -Wsuggest-final-types -Walloc-zero -Walloca -Wcast-align=strict -Wdate-time -Wdouble-promotion -Wduplicated-branches -Wduplicated-cond -Wsuggest-attribute=format -Wimplicit-fallthrough=5 -Wjump-misses-init -Wmultichar -Wpacked -Wshadow -Wshift-overflow=2 -Wstrict-overflow=5 -Wunused-macros -Wnull-dereference
ANALYZER_WARNINGS = -fanalyzer
CFLAGS = -std=c2x -O03 $(WARNINGS) $(ANALYZER_WARNINGS)
LIBS = #-lgsl -lgslcblas -lm

SOURCES = main.c
OBJECTS = $(SOURCES:.c=.o)
BUILDOBJS = $(addprefix $(OBJDIR)/,$(OBJECTS))

TARGET = minimal
BUILDTARGET = $(BUILDDIR)/$(TARGET)

MKDIR = mkdir -p $(dir $(DEPDIR)/$*) $(dir $(OBJDIR)/$*)
MKDEPEND = $(CC) -M -MT $(OBJDIR)/$(<:.c=.o) $(INCLUDES) -o $(DEPDIR)/$*.dep $<
MKDIRDEP = $(MKDIR) && $(MKDEPEND)

TITLE = @echo "----- $@"

.DEFAULT_GOAL := $(BUILDTARGET)

.PHONY: run
run: $(BUILDTARGET)
	$(BUILDTARGET) $(ARGS)

.PHONY: error
error:
	$(TITLE)
	$(MAKE) ERROR='-Werror'

$(OBJDIR)/%.o: %.c $(HEADERS)
	$(TITLE)
	$(MKDIRDEP)
	$(CC) -c -o $@ $< $(CFLAGS)

$(BUILDTARGET): $(BUILDOBJS)
	$(TITLE)
	$(CC) -o $@ $^ $(CFLAGS) $(LIBS)

.PHONY: clean
clean:
	rm -rf $(BUILDDIR)
